﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using studentFlex.Models;

namespace studentFlex.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<studentFlex.Models.Student> Student { get; set; }
        public DbSet<studentFlex.Models.Course> Course { get; set; }
        public DbSet<studentFlex.Models.Enrollment> Enrollment { get; set; }
    }
}
