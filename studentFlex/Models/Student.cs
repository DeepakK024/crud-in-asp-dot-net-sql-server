﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace studentFlex.Models
{
    public class Student
    {
        public int id { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public int age { get; set; }

        public string department { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public Student()
        {

        }
    }
}
