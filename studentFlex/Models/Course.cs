﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace studentFlex.Models
{
    public class Course
    {
        public int id { get; set; }

        public string name { get; set; }

        public string code { get; set; }

        public string description { get; set; }

        public string creditHourse { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public Course()
        {

        }
    }
}
